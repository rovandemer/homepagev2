// Mac animation
const macSection = document.querySelector('.mac-section');
const macAnimCanvas = macSection.querySelector('#mac-anim');
const downArrow = document.querySelector('#down-arrow');
const macAnimCanvasCtx = macAnimCanvas.getContext('2d');

const controller = new ScrollMagic.Controller();
const macbookAnimDuration = 1500;
const frameNumber = 33; // divide by 3 to increase stability
const img = new Image();
macAnimCanvas.height = 1080;
macAnimCanvas.width = 1920;

var verifyMobile = () => {
    if (document.documentElement.clientWidth < 768) {
        return true;
    }
    return false;
}

const isMobile = verifyMobile();

var posX = 0;
var posY = 0;

var frame = 0;
var scrollpos = 0;
var delay = 0;
var accelamount = 0.1;

const currentFrame = (index) => (
    `./assets/macbook-anim/${(index * 3).toString().padStart(4, '0')}.jpg`
)


img.src = currentFrame(1);
img.onload = () => {
    // Place image at the center of the canvas
    macAnimCanvasCtx.drawImage(img, posX, posY);
}

const updateImage = (index) => {
    img.src = currentFrame(index);
    macAnimCanvasCtx.drawImage(img, posX, posY);
}




if (!isMobile) {

    macSection.removeChild(downArrow);

    var macbookScene = new ScrollMagic.Scene({
        duration: macbookAnimDuration,
        triggerElement: macSection,
        triggerHook: 0
    })
    .setPin(macSection)
    .addTo(controller);

    macbookScene.on("update", e => {
        scrollpos = e.scrollPos / 1000;
    });
    
    setInterval(() => {
        delay += (scrollpos - delay) * accelamount;
    
        frame = Math.floor(delay * 10 * 10000 / macbookAnimDuration / 3);
    
        if (frame < frameNumber && frame > 0) {
            updateImage(frame);
        }
    }, 5);

} else {
    // remove macbook animation
    macSection.removeChild(macAnimCanvas);
}




// Project animation
const addFadeScene = (element, options = {}) => {
    // Remove opacity from element
    element.style.opacity = 0;

    var {
        duration="100%",
        triggerHook=1,
        offset=(isMobile) ? "-100%" : "100%",
    } = options;
    const sceneTl = new TimelineMax();
    sceneTl
    .set(element, { opacity: 0 })
    .to(element, 1, { opacity: 1 });


    const scene = new ScrollMagic.Scene({
        duration: duration,
        triggerElement: element,
        triggerHook: triggerHook,
        offset: offset,
    })
    .setTween(sceneTl)
    .addTo(controller);
    return scene;
}

const addProjectScene = (project) => {
    const projectTitle = project.querySelector('.project-title');
    const projectDesc = project.querySelector('.project-desc');
    const projectImg = project.querySelector('.project-img');
    const projectRepo = project.querySelector('.project-repo');
    const projectAuthors = project.querySelector('.project-authors');

    return {
        title: addFadeScene(projectTitle, { triggerHook: (isMobile) ? 0.6 : 0.8 }),
        desc: addFadeScene(projectDesc, { triggerHook: (isMobile) ? 0.6 : 0.8 }),
        img: addFadeScene(projectImg, { triggerHook: (isMobile) ? 0.6 : 0.8 }),
        repo: addFadeScene(projectRepo, { triggerHook: (isMobile) ? 0.7 : 0.9 }),
        authors: addFadeScene(projectAuthors, { triggerHook: (isMobile) ? 0.7 : 0.9 }),
    }
}


const yearTitle = document.querySelector('#year-name-2021');
const projectSectionTitle = document.querySelector('#project-section-title');
const projects = document.querySelectorAll('.project');


const yearTitleScene = addFadeScene(yearTitle, { triggerHook: 0.5 });
const projectSectionTitleScene = addFadeScene(projectSectionTitle, { triggerHook: 0.5 });

for (let i = 0; i < projects.length; i++) {
    const project = projects[i];
    const projectScene = addProjectScene(project);
}
